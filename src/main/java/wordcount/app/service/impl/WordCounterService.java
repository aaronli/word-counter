package wordcount.app.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import wordcount.app.data.Keyword;
import wordcount.app.service.CounterService;

/**
 * 
 * Service that handles the counting of words
 */
public class WordCounterService implements CounterService {
	
	/**
	 * Given a list of data strings and a list of keywords, count the number of occurences of each of the keywords
	 */
	public Set<Keyword> countWords(List<String> dataLines, List<String> keywordLines) {
		if (dataLines == null || dataLines.isEmpty()) {
			throw new IllegalArgumentException("Dataset is empty.");
		}
		if (keywordLines == null || keywordLines.isEmpty()) {
			throw new IllegalArgumentException("No keywords to count found.");
		}
		Set<Keyword> keywords = stringToKeyword(keywordLines);
		
		for (String dataLine : dataLines) {
			count(dataLine, keywords);
		}
		return keywords;
	}

	/**
	 * Transforms a list of words into a set of Keywords
	 */
	public Set<Keyword> stringToKeyword(List<String> words) {
		
		Set<Keyword> keywords = new HashSet<>();
		for (String word : words) {
			Keyword keyword = new Keyword(word.trim());
			keywords.add(keyword);
		}
		return keywords;
	}

	/**
	 * Counts the number of occurences of each of the Keywords in a given string
	 */
	public void count(String data, Set<Keyword> keywords) {
		
		for (Keyword keyword : keywords) {
			String key = keyword.getWord();
			String temp = data.toLowerCase();
			int originalLength = data.length();
			String tempString =  temp.replaceAll("\\b("+ key + ")\\b", "");
			int newLength = tempString.length();
			int occurences = (originalLength - newLength) / key.length();
			keyword.addOccurrences(occurences);
			
		}
	}

}
