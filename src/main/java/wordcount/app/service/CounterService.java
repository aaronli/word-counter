package wordcount.app.service;

import java.util.List;
import java.util.Set;

import wordcount.app.data.Keyword;

public interface CounterService {
	
	Set<Keyword> countWords(List<String> dataLines, List<String> keywordLines);
	
	Set<Keyword> stringToKeyword(List<String> words);
	
	void count(String data, Set<Keyword> keywords);

}
