package wordcount.app.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import wordcount.app.dao.DataAccessor;
import wordcount.app.data.Keyword;
import wordcount.app.service.CounterService;
import wordcount.app.service.impl.WordCounterService;

public class WordCounter {
	
	public static void main(String[] args) {

		String workingDirectory = System.getProperty("user.dir");
		String dataFile = "data.txt";
		String keywordsFile = "keywords.txt";
		
		CounterService counterService = new WordCounterService();
		DataAccessor dataAccessor = new DataAccessor();
		
		try {
			
			List<String> dataLines = dataAccessor.readFile(workingDirectory, dataFile);
			List<String> keywordLines = dataAccessor.readFile(workingDirectory, keywordsFile);
			
			List<Keyword> keywords = new ArrayList<>(counterService.countWords(dataLines, keywordLines));
			Collections.sort(keywords);
			
			for (int i = keywords.size()-1; i >= 0; i--) {
				Keyword temp = keywords.get(i);
				System.out.println(String.format("%s - %d", temp.getWord(), temp.getOccurrences()));
			}
			
		} catch (IOException ex) {
			System.err.println("Issue reading file : " + ex.getMessage());
		} catch(IllegalArgumentException ex) {
			System.err.println("Cannot count keywords : " + ex.getMessage());
		}
		
	}

}
