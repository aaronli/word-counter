package wordcount.app.data;

/**
 * Object that stores the keyword value as well as the occurences
 * @author ali
 *
 */
public class Keyword implements Comparable<Keyword>{

	private String word;
	private int occurrences;
	
	public Keyword(String word){
		this.word = word.toLowerCase();
		occurrences = 0;
	}
	
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(int occurences) {
		this.occurrences = occurences;
	}
	public void addOccurrences(int occurences){
		this.occurrences+=occurences;
	}
	
	@Override
	public int compareTo(Keyword keyboard) {
		return occurrences - keyboard.getOccurrences();
	}

}
