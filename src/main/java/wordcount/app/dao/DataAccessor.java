package wordcount.app.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Service that handles the reading of data
 *
 */
public class DataAccessor {

	public List<String> readFile(String directory, String input) throws IOException{
		return Files.readAllLines(Paths.get(directory, input));
	}
	
}
