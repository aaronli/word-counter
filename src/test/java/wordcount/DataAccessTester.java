package wordcount;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import wordcount.app.dao.DataAccessor;

public class DataAccessTester {
	
	DataAccessor da;
	String testingDirectory;
	
	@Before
	public void init() throws IOException{
		da = new DataAccessor();
		Resource resource= new ClassPathResource("testfile.txt");
		testingDirectory = resource.getFile().getParent();
	}

	@Test
	public void checkLines() throws IOException{
		List<String> lineCount = da.readFile(testingDirectory, "testfile.txt");
		assertEquals(10, lineCount.size());
	}

	@Test(expected = NoSuchFileException.class)
	public void checkExists() throws IOException{
		da.readFile(testingDirectory, "notestfile.txt");
	}
	
	@Test
	public void checkEmpty() throws IOException{
		List<String> lineCount = da.readFile(testingDirectory, "emptyfile.txt");
		assertEquals(0, lineCount.size());
	}
	
	
}

