package wordcount;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import wordcount.app.data.Keyword;
import wordcount.app.service.CounterService;
import wordcount.app.service.impl.WordCounterService;

public class CounterTester {

	List<String> keywords;
	CounterService wcs;
	
	@Before
	public void init(){
		 wcs = new WordCounterService();
		String[] keywords = {"hello","and","syntax"};
		this.keywords = new ArrayList<>();
		for(String keyword : keywords) {
			this.keywords.add(keyword);
		}
	}
	
	@Test	
	public void countTest(){
		
		String data = "Hello, World! program is a computer program that outputs or displays Hello, "
				+ "World! to a user. Being a very simple program in most programming languages, it is "
				+ "often used to illustrate the basic syntax of a programming language for a working "
				+ "program.[1] It is often the very first program people write when they are new to a language.";
		
		Set<Keyword> keywurds = wcs.stringToKeyword(keywords);
		wcs.count(data, keywurds);
		List<Keyword> wordList = new ArrayList<>(keywurds);
		Collections.sort(wordList);
		assertEquals("and", wordList.get(0).getWord());
		assertEquals(0, wordList.get(0).getOccurrences());
		assertEquals("syntax", wordList.get(1).getWord());
		assertEquals(1, wordList.get(1).getOccurrences());
		assertEquals("hello", wordList.get(2).getWord());
		assertEquals(2, wordList.get(2).getOccurrences());
	}
	
	@Test
	public void checkCase() {
		String data = "HELLO HELLO HELLO HELLO HELLO HELLO hello hello hello";
		Set<Keyword> keywurds = wcs.stringToKeyword(keywords);
		wcs.count(data, keywurds);
		List<Keyword> wordList = new ArrayList<>(keywurds);
		Collections.sort(wordList);
		assertEquals("hello", wordList.get(2).getWord());
		assertEquals(9, wordList.get(2).getOccurrences());
	}
	
}
